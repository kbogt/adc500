-- Primitive wrapper for Zynq Series 7
-- to be used in:

-- ADC500.vhd
-- ADC500_NO_FIFO.vhd

-- Description: Generic Data buffer differential intput to single ended.

-- Revision:
-- 0.01 - File created.

----------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Library UNISIM;
use UNISIM.vcomponents.all;

entity idatabuf is
    Generic(Nbits : integer :=16);
    Port(
    data_from_adc_DS_N : IN STD_LOGIC_VECTOR(Nbits-1 downto 0);
    data_from_adc_DS_P : IN STD_LOGIC_VECTOR(Nbits-1 downto 0);
    DATA    : OUT STD_LOGIC_VECTOR(Nbits-1 downto 0)
    );
end idatabuf;

architecture idatabuf_struct of idatabuf is

begin
    GEN_IBUFDS : for i in 0 to Nbits-1 generate

    IBUFDS_I : IBUFDS
    port map (O => DATA(i), I => data_from_adc_DS_P(i), IB => data_from_adc_DS_N(i));

 end generate GEN_IBUFDS;

end idatabuf_struct;