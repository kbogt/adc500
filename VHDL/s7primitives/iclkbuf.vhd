-- Primitive wrapper for Zynq Series 7
-- to be used in:

-- ADC500.vhd
-- ADC500_NO_FIFO.vhd

-- Descrpition: Differentical clock input buffer to single ended. 

-- Revision:
-- 0.01 - File created.

----------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Library UNISIM;
use UNISIM.vcomponents.all;

entity iclkbuf is
    Port(
        clk_in_p : IN std_logic;
        clk_in_n : IN std_logic;
        clk_o : OUT std_logic
    );
end iclkbuf;

architecture iclkbuf_struct of iclkbuf is
begin 

IBUFGDS_inst : IBUFGDS
port map (
O => clk_o, -- Clock buffer output
I => clk_in_p, -- Diff_p clock buffer input (connect directly to top-level port)
IB => clk_in_n -- Diff_n clock buffer input (connect directly to top-level port)
);

end iclkbuf_struct;