-- Primitive wrapper for Zynq Series 7
-- to be used in:

-- ADC500.vhd
-- ADC500_NO_FIFO.vhd

-- Description: Single ended clock input to differential output. 


-- Revision:
-- 0.01 - File created.

----------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

Library UNISIM;
use UNISIM.vcomponents.all;

entity oclkbuf is
    Port(
        clk_in : IN std_logic;
        clk_o_p : OUT std_logic;
        clk_o_n : OUT std_logic
    );
end oclkbuf;

architecture oclkbuf_struct of oclkbuf is
begin 

   OBUFDS_I : OBUFDS
   port map (O => clk_o_p, OB => clk_o_n, I => clk_in);

end oclkbuf_struct;