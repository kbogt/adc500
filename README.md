This driver is compatible with ICTP-INFN ADC500 board V1.0 and V0.1

Import ADC500.vhd for a complete driver, or ADC500_NO_FIFO.vhd for a simplify version. 
Import package and primitives from s7primitives.

An example with ILA is found in bd.

The constraint files for the example is found in const and tested for CIAA_ACC.

Documentation has information about pinout 



| Pin | Register bit| Description|
|----| ------|-----|
|x_to_adc_cal_fmc | Ctrl_reg_in(0)| Calibration Cycle High initiate Calibration |
|x_to_adc_caldly_nscs_fmc | Ctrl_reg_in(1)| Calibration Delay and Serial Interface Chip Select. |
|x_to_adc_fsr_ece_fmc | Ctrl_reg_in(2)| Full Scale Range Select and Extended Control Enable.|
|x_to_adc_outv_slck_fmc | Ctrl_reg_in(3)| Output Voltage Amplitude and Serial Interface Clock.| 
|x_to_adc_outedge_ddr_sdata_fmc | Ctrl_reg_in(4) |DCLK Edge Select, Double Data Rate Enable and Serial Data Input.|
|x_to_adc_dclk_rst_fmc | Ctrl_reg_in(5) |DCLK Reset.|
|x_to_adc_pd_fmc | Ctrl_reg_in(6)|Power Down|
|x_to_adc_led_0 | Ctrl_reg_in(7)|LED 0|
|x_to_adc_led_1 | Ctrl_reg_in(8)|LED 1|
|x_to_adc_reset_counter | Ctrl_reg_in(9)|Resets internal counter when this bit is asserted. Free-running counter otherwise.|

| Register bit| Pin | Description |
|-----------|-------------| ------ |
|Ctrl_reg_out(0) | x_from_adc_calrun_fmc|Calibration Running indication.|
|Ctrl_reg_out(1) | x_from_adc_or(0)| Out Of Range output |
|Ctrl_reg_out(31 downto 2) |(others=>'0')|


FIFO primitive issue: 

https://www.xilinx.com/support/answers/53373.html